# Lab 2 - Linar & SonarQube

Student name: **Marko Pezer** \
Student group: **BS18-SE-01**

Here are the screenshots:

![image info](./screenshots/sqr_lab2.PNG)

![image info](./screenshots/sqr_lab2_02.PNG)

![image info](./screenshots/sqr_lab2_03.PNG)

![image info](./screenshots/sqr_lab2_04.PNG)
